package org.testapp.client;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class MainClass {

    private static Logger log = Logger.getLogger(MainClass.class.getName());

    public static void main(String[] args) {

        configureLogging();
        String errMsg = "wrong parameters, enter server ip and port(int value, 1 to 65535), example: \"client localhost 8888\"";
        if (args.length >= 2){
            try{
                String hostname = args[0];
                int port = Integer.parseInt(args[1]);
                if (port < 1 || port > 65535){
                    log.log(Level.SEVERE, errMsg);
                    System.out.println(errMsg);
                    System.exit(-1);
                }
                Client client = new Client(hostname, port);
                client.start();
            }
            catch (NumberFormatException e){
                log.log(Level.SEVERE, errMsg);
                System.out.println(errMsg);
                System.exit(-1);
            }
        }
        else{
            log.log(Level.SEVERE, errMsg);
            System.out.println(errMsg);
        }
    }
    public static void configureLogging(){
        FileHandler fh;

        try {
            fh = new FileHandler("server.log", 1000000, 10);
            log.addHandler(fh);
            SimpleFormatter formatter = new SimpleFormatter();
            fh.setFormatter(formatter);
            log.setUseParentHandlers(false);
        } catch (SecurityException | IOException e) {
            e.printStackTrace();
        }
    }

    public static Logger getLog() {
        return log;
    }
}
