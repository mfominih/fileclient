package org.testapp.client;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class Client {

    public static final int STATUS_SENDING_MESSAGE = 1;
    public static final int STATUS_SENDING_FILE = 2;
    public static final int STATUS_WRONG_FILE_NUMBER = 3;
    public static final int STATUS_FILE_DOWNLOADED = 4;
    public static final int STATUS_SERVER_ERROR = 5;
    public static final int STATUS_REQUEST_FILE_LIST = 6;
    public static final int STATUS_REQUEST_FILE = 7;
    public static final int STATUS_FILE_IS_NOT_DOWNLOADED = 8;

    private Socket socket;
    private DataInputStream in;
    private DataOutputStream out;

    private Logger log = MainClass.getLog();

    /**
     *
     * @param hostname server address
     * @param port
     */
    public Client(String hostname, int port) {
        try {
            socket = new Socket(hostname, port);
            in = new DataInputStream(socket.getInputStream());
            out = new DataOutputStream(socket.getOutputStream());
        } catch (IOException ex) {
            System.out.println("Can't connect to " + hostname + ":" + port + " (usage: client hostname port)");
            log.log(Level.SEVERE, ex.getMessage(), ex);
            closeConnection();
        }
    }

    public void start(){
        Scanner scanner = new Scanner(System.in);
        initCycle:
        while (true) {
            System.out.println("choose variant");
            System.out.println("1. show file list");
            System.out.println("2. get file");
            System.out.println("3. close program");

            while (true) {
                //read selected option
                String chosenVariant = scanner.nextLine();
                switch (chosenVariant) {
                    //get file list
                    case "1":
                        try {
                            log.log(Level.INFO, "loading file list");
                            String fileList = requestFileList();
                            System.out.println("File list:");
                            System.out.println(fileList);
                        } catch (IOException e) {
                            log.log(Level.SEVERE, e.getMessage(), e);
                            closeConnection();
                        }
                        break;
                    //get file
                    case "2":
                        try {
                            String fileList = requestFileList();
                            System.out.println("File list:");
                            System.out.println(fileList);
                            System.out.print("enter file number or 0 to cancel: ");
                            int fileNumber;
                            while (true) {
                                try {
                                    fileNumber = scanner.nextInt();
                                    if (fileNumber < 0) {
                                        throw new InputMismatchException("value must be greater than or equals 0");
                                    }
                                    if (fileNumber == 0) {
                                        continue initCycle;
                                    }
                                    break;
                                } catch (InputMismatchException e) {
                                    log.log(Level.SEVERE, e.getMessage(), e);
                                    System.out.println("Entered value is not correct");
                                }
                            }
                            log.log(Level.INFO, "loading file");
                            downloadFile(fileNumber);
                        } catch (IOException e) {
                            log.log(Level.SEVERE, e.getMessage(), e);
                            closeConnection();
                        }
                        break;
                    //close connection
                    case "3":
                        closeConnection();
                        break;
                    default:
                        continue;
                }
                break;
            }
        }
    }

    private String requestFileList() throws IOException {
        sendStatus(STATUS_REQUEST_FILE_LIST);
        int state = readStatus();
        if (state == STATUS_SENDING_MESSAGE){
            return getMessage();
        }
        throw new IOException("cannot read message from server");
    }


    /**
     * send client status to server
     * @param status
     */
    private void sendStatus(int status){
        try {
            out.writeInt(status);
            out.flush();
        } catch (IOException ex) {
            log.log(Level.SEVERE, ex.getMessage(), ex);
            ex.printStackTrace();
        }
    }

    /**
     * read status from server
     */
    private int readStatus() throws IOException {
        int state = in.readInt();
        if (STATUS_SERVER_ERROR == state){
            System.out.println("Server error");
            log.log(Level.SEVERE, "Server error");
            closeConnection();
        }
        return state;

    }

    /**
     * send selected file number
     * @param fileNumber
     */
    private void sendFileNumber(int fileNumber){
        try {
            out.writeInt(fileNumber);
            out.flush();
        } catch (IOException ex) {
            log.log(Level.SEVERE, ex.getMessage(), ex);
            ex.printStackTrace();
        }
    }

    /**
     *
     * @return text message from server
     */
    public String getMessage(){
        String msg = null;
        try {
            msg = in.readUTF();
        } catch (IOException ex) {
            log.log(Level.SEVERE, "error while getting a message", ex);
            System.out.println("error while getting a message");
            closeConnection();
        }
        return msg;
    }

    /**
     *
     * @param fileName init file name
     * @return result file name
     */

    private String defineSavedFileName(String fileName){
        int count = 0;
        String newFileName = fileName;
        String[] nameParts = fileName.split("\\.");
        while ((new File(newFileName)).exists()){
            if (nameParts.length == 1){
                newFileName = fileName + count;
                continue;
            }
            count++;
            StringBuilder sb = new StringBuilder(nameParts[0]);
            for (int i = 1; i < nameParts.length; i++)
            {
                if (i == (nameParts.length - 1)) sb.append("(" + count + ")");
                sb.append("." + nameParts[i]);

            }
            newFileName = sb.toString();
        }
        return newFileName;
    }

    /**
     * get file data
     * @param fileNumber
     */
    private void downloadFile(int fileNumber){
        System.out.println("type path to save file (empty for current directory)\n");
        String fileName;
        sendStatus(STATUS_REQUEST_FILE);
        //sending selected file number
        sendFileNumber(fileNumber);
        try {
            int state = readStatus();
            if (state == STATUS_WRONG_FILE_NUMBER){
                System.out.println(String.format("file with number %s not found", fileNumber));
                return;
            }else if (state != STATUS_SENDING_FILE) {
                throw new IOException("Error while loading file");
            }
        } catch (IOException e) {
            log.log(Level.SEVERE, e.getMessage(), e);
            return;
        }
        //define save path
        String pathDir;
        while (true){
            Scanner scanner = new Scanner(System.in);
            pathDir = scanner.nextLine();
            File file = new File(pathDir);
            if (pathDir.isEmpty() || (file.exists() && file.isDirectory())){
                break;
            }
            System.out.println("wrong path, enter correct value\n");
        }
        if ("Linux".equals(System.getProperty("os.name")) && !pathDir.endsWith("/") && !pathDir.isEmpty()) pathDir +="/";
        if ("Windows".equals(System.getProperty("os.name")) && !pathDir.endsWith("\\") && !pathDir.isEmpty()) pathDir +="\\";
        long fileSize;
        try {
            fileSize = in.readLong();
            fileName = defineSavedFileName(pathDir + in.readUTF());
            File file = new File(fileName);
            FileOutputStream fos = new FileOutputStream(file);
            int length, total = 0;
            byte[] buffer = new byte[1024];
            System.out.println();

            long tmpSize = fileSize/100;
            int pointCount = 1;
            if (fileSize > 0) {
                while ((length = in.read(buffer)) != -1) {

                    total += length;
                    fos.write(buffer, 0, length);
                    if (total >= tmpSize * pointCount) {
                        System.out.print(".");
                        pointCount++;
                    }
                    if (total == fileSize) {
                        System.out.println();
                        break;
                    }
                }
            }
            fos.flush();
            fos.close();
            System.out.print("file " + file.getAbsolutePath() + " is saved\n");
            log.log(Level.INFO, "file is downloaded");
            sendStatus(STATUS_FILE_DOWNLOADED);
        } catch (IOException ex) {
            sendStatus(STATUS_FILE_IS_NOT_DOWNLOADED);
            System.out.println("error while getting file");
            log.log(Level.SEVERE, ex.getMessage(), ex);
            closeConnection();
        }
    }

    /**
     * close socket and exit rpogram
     */
    public final void closeConnection(){

        log.log(Level.INFO, "Closing connection");
        try {
            socket.close();
            System.exit(-1);
        } catch (IOException ex) {
            log.log(Level.SEVERE, ex.getMessage(), ex);
            System.exit(-1);
        }
    }
}
